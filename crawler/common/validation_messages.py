class Message:
    WrongCrawlingDepth = 'you entered incorrect crawling depth {0}.'
    UnableToDownload = 'Unable to download the page content for {0}.'
    DownloadFailed = 'Download failed with status code: {0}. try later'
    EmptyContent = 'Content should not be empty'
    EmptyEntity = 'the parameter ({0}) should not be empty.'
    GeneralServerFail = 'Server failed to operate. please contact the administrators.'
    InvalidUrlFormat = '{0} has invalid url format.'

