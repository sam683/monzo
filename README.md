## Monzo web crawler
Multi threaded web crawler in Django app using python 3.6


### Description:
Monzo web crawler is a Django rest service that accepts a url as root page and crawls its nodes based on requested depth of
crawling and in return, outputs a json file. the output data structure is shown below:

```
{  
   "status":true,
   "data":{  
      "url":"https://www.monzo.com",
      "css_links":[  
            "/static/css/home.css?v21",
            "/static/css/font-awesome.min.css",
            ..
      ],
      "script_links":[  
            "/static/scripts/modernizr.min.js",
            "/static/scripts/optimizely_5973193136.js",
            ..
      ],
      "image_links":[  
            "/static/images/home/v3/bank-love_uw2hzy_c_scale,w_792.png",
            "/static/images/home/v3/send-money_ype0cq_c_scale,w_1016.png",
		 ...
      ],
      "pages":[  
		...
      ]
   }
}
```

### API specification

Address: (default Django port - 8000)
```
[GET] http://127.0.0.1:8000/crawl/
```

Input: input data is a an encoded querystring which holds two parametrers: url and crawling_depth
```
parameters: {'url': 'https://www.monzo.com', 'crawling_depth': 1}

example: http://127.0.0.1:8000/crawl/?url=https%3A%2F%2Fwww.monzo.com&crawling_depth=1

```


### install
```
    python version 3.6
    virtualenv env/
    source env/bin/activate
    pip install -r requirements.txt
    python manage.py runserver

```

### test
```
python test_crawler.py
```
if you run the tests in windows, set the python path:
```
set PYTHONPATH=C:\Users\usr\.. path to project
```